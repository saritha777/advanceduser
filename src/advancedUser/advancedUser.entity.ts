import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  public id: number;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The first name of the Advanced user',
    default: ''
  })
  @Column()
  public firstName: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The last name of the Advanced user',
    default: ''
  })
  @Column()
  public lastName: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The dob of the Advanced user',
    default: ''
  })
  @Column()
  public dob: string;

  @IsInt()
  @ApiProperty({
    type: Number,
    description: 'The age of the Advanced user',
    default: ''
  })
  @Column()
  public age: number;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The email of the Advanced user',
    default: ''
  })
  @Column({ unique: true })
  public email: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The email of the Advanced user',
    default: ''
  })
  @Column()
  public password: string;

  @IsInt()
  @ApiProperty({
    type: Number,
    description: 'The phone number of the Advanced user',
    default: ''
  })
  @Column()
  public phoneNumber: number;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The address of the Advanced user',
    default: ''
  })
  @Column()
  public address: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The state of the Advanced user',
    default: ''
  })
  @Column()
  public state: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The country of the Advanced user',
    default: ''
  })
  @Column()
  public country: string;

  @IsInt()
  @ApiProperty({
    type: Number,
    description: 'The pincode of the Advanced user',
    default: ''
  })
  @Column()
  public pinCode: number;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;
}
