import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './advancedUser.entity';
import * as bcrypt from 'bcryptjs';
import { UserLogin } from './login.entity';
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express';

@Injectable()
export class AdvancedUserService {
  logger: Logger;
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private jwtService: JwtService //  @InjectRepository(UserLogin)
  ) //  private loginRepository: Repository<UserLogin>
  {
    this.logger = new Logger(AdvancedUserService.name);
  }

  getUser(): Promise<User[]> {
    this.logger.log('Getting all users');
    return this.userRepository.find();
  }

  getUserByEmail(email: string): Promise<User> {
    this.logger.log('Getting user with email');
    return this.userRepository.findOne({ email });
  }
  getUserById(id: number): Promise<User> {
    this.logger.log('Getting user with id');
    return this.userRepository.findOne({ id });
  }

  async addUser(data: User): Promise<User> {
    // return this.userRepository.save(user);
    const pass = await bcrypt.hash(data.password, 10);
    const user = new User();
    const login = new UserLogin();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.dob = data.dob;
    user.age = data.age;
    user.email = login.email = data.email;
    user.password = login.password = pass;
    user.phoneNumber = data.phoneNumber;
    user.address = data.address;
    user.state = data.state;
    user.country = data.country;
    user.pinCode = data.pinCode;
    user.login = login;
    //  delete user.password;
    return this.userRepository.save(user);
  }

  //checking user is logined or not
  async findOne(data: UserLogin, @Res() response: Response) {
    const user = await this.userRepository.findOne({ email: data.email });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('pasward incorrect');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  // find user using cookie
  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.userRepository.findOne({ id: data['id'] });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
  async updateUser(id: number, user: User): Promise<User> {
    await this.userRepository.update(id, user);
    return this.userRepository.findOne({ id });
  }

  deleteUser(email: string) {
    return this.userRepository.delete({ email });
  }

  deleteUserById(id: number) {
    return this.userRepository.delete({ id });
  }

  //logout user
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout'
    };
  }
}
