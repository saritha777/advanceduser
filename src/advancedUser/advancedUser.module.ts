import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdvancedUserController } from './advancedUser.controller';
import { User } from './advancedUser.entity';
import { AdvancedUserService } from './advancedUser.service';
import { UserLogin } from './login.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLogin]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [AdvancedUserController],
  providers: [AdvancedUserService]
})
export class AdvancedUserModule {}
