import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
  UnauthorizedException,
  UseFilters,
  UsePipes
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiForbiddenResponse,
  ApiTags,
  ApiCreatedResponse,
  ApiBody
} from '@nestjs/swagger';
import { ValidationPipes } from '../pipes/validation.pipe';
import { HttpExceptionFilter } from '../filters/http-exception.filter';
import { AdvancedUserService } from './advancedUser.service';
import { UserData } from '../decorators/user.decorator';
import { User } from './advancedUser.entity';
import * as bcrypt from 'bcryptjs';
import { UserLogin } from './login.entity';
import { Response, Request } from 'express';
import { customException } from '../customException/custom.exception';

@ApiTags('user')
@Controller('user')
//Global level Exceptionv
//@UseFilters(HttpExceptionFilter)
@UsePipes(ValidationPipes)
export class AdvancedUserController {
  constructor(private readonly userService: AdvancedUserService) {}

  @Get()
  @ApiOkResponse({
    description: ' The resource list has been successfully returned.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  getUser(): Promise<User[]> {
    return this.userService.getUser();
  }

  @Get('/email/:email')
  @ApiOkResponse({
    description: ' The resource list has been successfully returned.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  getUserByEmail(@Param('email') email: string): Promise<User> {
    return this.userService
      .getUserByEmail(email)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new customException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Get('/id/:id')
  //method level exception
  // @UseFilters(HttpExceptionFilter)
  @ApiOkResponse({
    description: ' The resource list has been successfully returned.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  getUserById(@Param('id') id: number): Promise<User> {
    return this.userService
      .getUserById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new customException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Post()
  @ApiBody({ type: User })
  // @UsePipes(ValidationPipes)
  @ApiCreatedResponse({
    description: ' The resource list has been successfully created.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  async postUser(@UserData() user: User) {
    // return this.userService.addUser(user);
    return this.userService.addUser(user);
    //console.log(user);
  }

  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.userService.findOne(login, response);
  }

  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.userService.findUser(request);
  }

  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.userService.logout(response);
  }

  @Put('/:id')
  @ApiOkResponse({
    description: ' The resource list has been successfully updated.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  updateUser(@Param('id') id: number, @Body() user: User): Promise<User> {
    return this.userService.updateUser(id, user);
  }

  @Delete('email/:email')
  @ApiOkResponse({
    description: ' The resource list has been successfully removed.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  deleteUser(@Param('email') email: string) {
    return this.userService.deleteUser(email);
  }

  @Delete('id/:id')
  @ApiOkResponse({
    description: ' The resource list has been successfully removed.'
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  deleteUserById(@Param('id') id: number) {
    return this.userService.deleteUserById(id);
  }
}
