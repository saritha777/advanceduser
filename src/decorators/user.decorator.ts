// import { createParamDecorator } from "@nestjs/common";

// export const UserData = createParamDecorator( (data:string, req) => {
//     return data? req.body && req.body[data]: req.body;
// });

import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const UserData = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.body;

    return data ? user && user[data] : user;
  }
);
