'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">advanceduser documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdvancedUserModule.html" data-type="entity-link" >AdvancedUserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' : 'data-target="#xs-controllers-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' :
                                            'id="xs-controllers-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' }>
                                            <li class="link">
                                                <a href="controllers/AdvancedUserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdvancedUserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' : 'data-target="#xs-injectables-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' :
                                        'id="xs-injectables-links-module-AdvancedUserModule-a19970d340a1b9cb2900a482b109d9167fbe5457fb4c66614986173d1f9bfb8cc41e50cd7c90b0da52c28c5b7c8429d06e85caa9025fea7e268cb6027d6dd648"' }>
                                        <li class="link">
                                            <a href="injectables/AdvancedUserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdvancedUserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' : 'data-target="#xs-controllers-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' :
                                            'id="xs-controllers-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' : 'data-target="#xs-injectables-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' :
                                        'id="xs-injectables-links-module-AppModule-08510a48ab25039f3e20fe1796ace51d9f18005a3424cbd83cc875fb58eea31845d0d4075cf2c903f48072903b3420979eb10771b5b4b9b81fbad1867c77c56d"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AdvancedUserController.html" data-type="entity-link" >AdvancedUserController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/customException.html" data-type="entity-link" >customException</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserLogin.html" data-type="entity-link" >UserLogin</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AdvancedUserService.html" data-type="entity-link" >AdvancedUserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});